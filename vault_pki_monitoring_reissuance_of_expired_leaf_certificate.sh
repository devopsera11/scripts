#!/bin/bash

# Define a variable for the Vault token
VAULT_TOKEN="hvs.welcome1234"

# Array to store serial IDs for which certificates have been reissued
reissued_certificates=()

echo "Starting script..."

# Function to execute cURL command for each serial ID
get_certificates() {
    while IFS= read -r serial_id; do
        echo "Processing serial ID: $serial_id"

        # Check if the certificate has been reissued, if yes, skip reissue check
        if [[ " ${reissued_certificates[@]} " =~ " $serial_id " ]]; then
            echo "Certificate for serial ID $serial_id has already been reissued. Skipping..."
            continue
        fi
        
        # Execute cURL command for the current serial ID using the VAULT_TOKEN variable
        response=$(curl -s -X 'GET' "http://localhost:8200/v1/pki_intermediate/cert/$serial_id" \
        -H 'accept: application/json' \
        -H "X-Vault-Token: $VAULT_TOKEN")

        # Extract certificate from the response
        certificate=$(echo "$response" | jq -r '.data.certificate')

        # Save certificate to a file
        echo -e "$certificate" > "Certificate_$serial_id.pem"
        openssl x509 -in "Certificate_$serial_id.pem" -noout -subject -text | awk '/Subject:/,/X509v3 Subject Alternative Name/ { if(/Subject:/) { sub(/^.*CN=/, "", $0); printf "Subject: %s\n", $0 } if(/X509v3 Subject Alternative Name/) { getline; printf "SANs: %s\n", $0 } }' > "Subject_SANs_$serial_id.txt"

        # Use OpenSSL to check the certificate's expiration date directly
        not_after=$(openssl x509 -enddate -noout -in "Certificate_$serial_id.pem" | cut -d= -f2)
        expiration_date=$(date -ud "$not_after" +%s)
        current_date=$(date +%s)

        if [ "$current_date" -gt "$expiration_date" ]; then
            echo "Certificate for serial ID $serial_id is expired. Processing..."
            reissue_certificate "$serial_id"
        else
            echo "Certificate for serial ID $serial_id is not expired or no expiration info. Skipping..."
        fi

        echo "Finished processing serial ID: $serial_id"
    done < "serial_ids.txt"
}

reissue_certificate() {
    local serial_id=$1
    local common_name=$(awk '/Subject:/ {sub(/^.*CN=/, "", $0); print $NF}' "Subject_SANs_$serial_id.txt")

    response=$(curl -s 'http://localhost:8200/v1/pki_intermediate/issue/securosys-dot-ch_webserver-role' \
    -H 'Accept: application/json' \
    -H "X-Vault-Token: $VAULT_TOKEN" \
    --data-raw "{\"common_name\":\"$common_name\",\"exclude_cn_from_sans\":false,\"format\":\"pem\",\"private_key_format\":\"der\",\"ttl\":\"10s\"}")

    private_key=$(echo "$response" | jq -r '.data.private_key')
    
    # Store private key in Vault
    curl -X POST "http://localhost:8200/v1/kv/data/private_key/$common_name" \
         -H "accept: application/json" \
         -H "Content-Type: application/json" \
         -H "X-Vault-Token: $VAULT_TOKEN" \
         --data-raw "{\"data\": {\"private_key\": $(jq -Rs . <<< "$private_key")}}"

    # Extract new serial ID from the response
    new_serial_id=$(echo "$response" | jq -r '.data.serial_number')

    # Add the new serial ID to the array of reissued certificates
    reissued_certificates+=("$new_serial_id")
}

# Execute the original cURL command to get serial IDs using the VAULT_TOKEN variable
response=$(curl -s -k --tlsv1.2 -X 'GET' 'http://localhost:8200/v1/pki_intermediate/certs/?list=true' \
-H 'accept: application/json' \
-H "X-Vault-Token: $VAULT_TOKEN")

# Extract serial IDs from the JSON response
echo "$response" | jq -r '.data.keys[]' > serial_ids.txt

# Process the certificates
get_certificates

# Delete generated PEM files and serial_ids.txt
rm -f Certificate_*.pem serial_ids.txt
rm -f Subject_SANs_*.txt

echo "Script completed."
